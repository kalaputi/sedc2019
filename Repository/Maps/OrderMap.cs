﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Maps
{
    public class OrderMap : EntityTypeConfiguration<Order>
    {
        public OrderMap()
        {

            this.Ignore(t => t.CreatedBy);
            this.Ignore(t => t.CreatedOn);
            this.Ignore(t => t.LastModifiedOn);
            this.Ignore(t => t.LastModifiedBy); 

            this.HasKey(t => t.ID);
            this.Property(t => t.ShipName).IsRequired().HasMaxLength(500);
            this.Property(t => t.ShipAddress).IsRequired().HasMaxLength(200);
            this.Property(t => t.ShipCity).IsRequired().HasMaxLength(100);
            this.Property(t => t.ShipPostalCode).IsRequired().HasMaxLength(50);
            this.Property(t => t.ShipCountry).IsRequired().HasMaxLength(100);



            this.HasMany(e => e.OrderDetails)
               .WithRequired(e => e.Order)
               .WillCascadeOnDelete(false);
        }
    }
}
