﻿using Entities.DbEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Maps
{
   public class CustomerMessageMap : EntityTypeConfiguration<CustomerMessage>
    {
        public CustomerMessageMap()
        {

            this.Ignore(t => t.CreatedBy);
            this.Ignore(t => t.CreatedOn);
            this.Ignore(t => t.LastModifiedOn);
            this.Ignore(t => t.LastModifiedBy);

            this.HasKey(t => t.ID);
            this.Property(t => t.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.Date).IsRequired();
            this.Property(t => t.Message).IsRequired();
            this.Property(t => t.Topic).IsRequired();

        }
    }
}
