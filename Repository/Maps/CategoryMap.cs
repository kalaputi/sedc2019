﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;

namespace Repository.Maps
{
    public class CategoryMap : EntityTypeConfiguration<Category>
    {
        public CategoryMap()
        {

            //ignore the baseentity

                this.Ignore(t => t.CreatedBy);
                this.Ignore(t => t.CreatedOn);
                this.Ignore(t => t.LastModifiedOn);
                this.Ignore(t => t.LastModifiedBy);

            this.HasKey(t => t.ID);
                this.Property(t => t.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
                this.Property(t => t.Name).IsRequired().HasMaxLength(500);

                this.HasMany(e => e.Products)
                .WithRequired(e => e.Category)
                .WillCascadeOnDelete(false);
        }
    }
}
