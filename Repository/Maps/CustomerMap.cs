﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Maps
{
   public class CustomerMap : EntityTypeConfiguration<Customer>
    {
        public CustomerMap()
        {

            this.Ignore(t => t.CreatedBy);
            this.Ignore(t => t.CreatedOn);
            this.Ignore(t => t.LastModifiedOn);
            this.Ignore(t => t.LastModifiedBy);

            this.HasKey(t => t.ID);
            this.Property(t => t.ID).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.CompanyName).IsRequired().HasMaxLength(500);
            this.Property(t => t.ContactName).IsRequired().HasMaxLength(100);
            this.Property(t => t.Address).IsRequired().HasMaxLength(200);
            this.Property(t => t.City).IsRequired().HasMaxLength(100);
            this.Property(t => t.PostalCode).IsRequired().HasMaxLength(50);
            this.Property(t => t.Country).IsRequired().HasMaxLength(100);
            this.Property(t => t.Phone).IsRequired().HasMaxLength(50);

            this.HasMany(e => e.Orders)
                .WithRequired(e => e.Customer)
                .WillCascadeOnDelete(false);

            this.HasMany(e => e.CustomerMessages)
                .WithRequired(e => e.Customer)
                .HasForeignKey(e=>e.CustomerID)
                .WillCascadeOnDelete(false);
        }
    }
}
