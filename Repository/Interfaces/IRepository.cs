﻿using Entities.DbEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Interfaces
{
   public interface IRepository<T> where T : BaseEntity
       //we do this because we want to protect ourselfs, we want to limit it to BaseEntity
    {
        void Add(T entity);
        //we define interface for categories
        //add the operations
        void Edit(T entity);

        void Delete(T entity);

        IEnumerable<T> All();

        IEnumerable<T> All(Expression<Func<T, bool>> predicate);

        void SaveChanges();//wtf baraj resenie vo IService
    }
}
