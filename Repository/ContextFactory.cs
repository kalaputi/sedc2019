﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Repository
{
   public class ContextFactory
    {
        public static string dbContextKey = "CompanyDB";
        //this code will not really work for windows service
        //http context for now is only for web
        //have a context in http
        public static CompanyDB GetDBContext()
        {
            if (HttpContext.Current.Items[dbContextKey] != null) {
                return (CompanyDB)HttpContext.Current.Items[dbContextKey];
            }
            else
            {

                var context = new CompanyDB();
                HttpContext.Current.Items[dbContextKey] = context;
               
                return context;
            }
        }
    }
}
