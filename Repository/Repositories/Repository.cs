﻿using Entities.DbEntities;
using Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
       

        public CompanyDB DataContext { get { return ContextFactory.GetDBContext(); } }

        public virtual void Add(T entity)
        {
            DataContext.Set<T>().Add(entity);
           // DataContext.SaveChanges();
        }

        public virtual IEnumerable<T> All()
        {
            return DataContext.Set<T>().AsEnumerable();
        }

        public virtual IEnumerable<T> All(Expression<Func<T, bool>> predicate)
        {
            return DataContext.Set<T>().Where(predicate).AsEnumerable();
        }

        public virtual void Delete(T entity)
        {
            DataContext.Set<T>().Remove(entity);
            //DataContext.SaveChanges();
        }

        public virtual void Edit(T entity)
        {
            //DataContext.SaveChanges();
        }

        public virtual void SaveChanges()
        {
            //moze da go koristime ova od ramkite na servisot

            DataContext.SaveChanges();
        }
    }
}
