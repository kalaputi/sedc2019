﻿using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DbEntities
{
   public partial class CustomerMessage: BaseEntity
    {
        

        public long ID { get; set; }

        public DateTime Date { get; set; }

        public string Message { get; set; }

        public string Topic { get; set; }

        public int CustomerID { get; set; }

        public virtual Customer Customer { get; set; }

        
    }
}
