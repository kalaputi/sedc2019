﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities.DbEntities
{
    public class BaseEntity
    {

        //this is good practice

        public DateTime CreatedOn { get; set; }

        public DateTime LastModifiedOn { get; set; }

        public DateTime LastModifiedBy { get; set; }

        public string CreatedBy { get; set; }
    }
}
