﻿using Entities.DbEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Services.Interfaces
{
    public interface IService<T> where T : BaseEntity
        //we do this because we want to protect ourselfs, we want to limit it to BaseEntity
    {
       
        void Create(T entity);
        //we define interface for categories
        //add the operations

        void Create(List<T> entities);

        void Update(T entity);

        void Delete(T entity);

        IEnumerable<T> GetAll();

        IEnumerable<T> GetAll(Expression<Func<T, bool>> predicate);

        //I don't need to upravuvam so kontekstot, servisot ke upravuvat zato neame Save CHanges
        //void SaveChanges();
    }
}
