﻿using Repository;
using Repository.Interfaces;
using Repository.Repositories;
using Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Services
{
  public  class CategoryService : Service<Category, ICategoryRepository>, ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        //we need a constructor for the repository to set, otherwise if its null it will go dooown
        //in repositories everything is virtual
        public override ICategoryRepository Repository
        {
            get; protected set;
        }

        public CategoryService(ICategoryRepository categoryRepository)
        {
            _categoryRepository = categoryRepository;
            Repository = _categoryRepository;
        }
        //segde kaj so ke najt IcaetgoryRepos da stajt categoryRepository
        //da ne e zavisna od klasa

    }
}
