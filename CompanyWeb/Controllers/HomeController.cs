﻿
using Services.Interfaces;
using Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CompanyWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IReportService _reportService;
        private readonly ICategoryService _categoryService;

        public HomeController(IReportService reportService, ICategoryService categoryService)
        {
           _reportService = reportService;
           _categoryService = categoryService;
        }

        public ActionResult Index()
        {
            //autofac
            //use the interface
            //ICategoryRepository _rep = new CategoryRepository();
            //var list = _rep.All(t=>t.ID == 1).ToList();

            //we dont want to use repositories in the controller
            //services is a business layer, only the services to be instanced
            //controllers are used to build the services
            //bad thing: repositories will be duplicated in the services as well
            //2 types of services: opsti services and 
            //services that will replace the repositories in the controller


            //from  last class
            //ICustomerRepository _rep = new CustomerRepository();
            //IOrderRepository _repo = new OrderRepository();

            //var list = _rep.All(t => t.ID == 1).ToList();
            //var orderlist = _repo.All().ToList();

            //new class

            //I have just added a comment

            var x = _reportService.GetCustomerCategories(1);
            var z = _categoryService.GetAll().ToList();

            return View();
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}