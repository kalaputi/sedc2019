﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Repository.Interfaces;
using Repository.Repositories;
using Services.Interfaces;
using Services.Services;


namespace CompanyWeb
{
    public static class Bootstrapper
    {
        public static void Run()
        {
            AutofacContainerBuilder();
        }

        public static void AutofacContainerBuilder()
        {
            var builder = new ContainerBuilder();

            builder.RegisterControllers(Assembly.GetExecutingAssembly());//vo ramkite na aplikacijata da gi re
            //gistrirat site kontroleri
            //site rep i servici so sakaame vo autofac da ni se registriret

            //mesto ona so repos i servisi mojs da mu recis se so zavrsvit na repository zameni go so soodvetniot interface
            //zemi go proektot
            var repositoryAssembly = Assembly.Load("Repository");
            builder.RegisterAssemblyTypes(repositoryAssembly)
                .Where(t => t.Name.EndsWith("Repository")).AsImplementedInterfaces().InstancePerRequest();

            //repositories
            //builder.RegisterType<CategoryRepository>().As<ICategoryRepository>().InstancePerRequest();
            //builder.RegisterType<CustomerRepository>().As<ICustomerRepository>().InstancePerRequest();
            //services

            var serviceAssembly = Assembly.Load("Services");
            builder.RegisterAssemblyTypes(serviceAssembly)
                .Where(t => t.Name.EndsWith("Service")).AsImplementedInterfaces().InstancePerRequest();
            //builder.RegisterType<ReportService>().As<IReportService>().InstancePerRequest();
            //builder.RegisterType<CategoryService>().As<ICategoryService>().InstancePerRequest();


            builder.RegisterFilterProvider();

            IContainer container = builder.Build();



            //samo ovoj resolver e od mvc, mu vikat, zemi koristi go resolverov od autofac
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}